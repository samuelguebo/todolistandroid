package colibri.com.apiclient;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends Activity {

    private RequestQueue queue;  // this = context
    private EditText taskTitle;
    private EditText taskDescription;
    private Button sendButton;
    private ProgressBar loadBar;
    private static String apiUrl ="https://apiclient.herokuapp.com/api/v1/Task/add";
    private Context mContext;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        queue = Volley.newRequestQueue(mContext);
        taskTitle = (EditText) findViewById(R.id.editLabel);
        taskDescription = (EditText) findViewById(R.id.editDescription);
        sendButton = (Button) findViewById(R.id.sendButton);
        loadBar=(ProgressBar)findViewById(R.id.progressBar1);
        loadBar.setVisibility(View.GONE);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendToApi();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void sendToApi(){
        loadBar.setVisibility(View.VISIBLE);
        StringRequest postRequest = new StringRequest(Request.Method.POST, apiUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        Toast.makeText(mContext, response.toString(), Toast.LENGTH_LONG).show();
                        loadBar.setVisibility(View.GONE);

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", String.valueOf(error));
                        Toast.makeText(mContext,String.valueOf(error), Toast.LENGTH_LONG).show();
                        loadBar.setVisibility(View.GONE);

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("label", taskTitle.getText().toString());
                params.put("description", taskDescription.getText().toString());

                return params;
            }
        };
        queue.add(postRequest);
    }
}
